class KalmanFilter {
  constructor(F, H, x0, P0, Q, R) {
    this.F = F;
    this.H = H;
    this.x = x0;
    this.P = P0;
    this.Q = Q;
    this.R = R;
  }

  predict() {
    this.x = mulMatrixVector(this.F, this.x);
    this.P = mulMatrixMatrix(mulMatrixMatrix(this.F, this.P), transposeMatrix(this.F));
    this.P = addMatrices(this.P, this.Q);

    return {
      x: this.x,
      P: this.P
    };
  }

  update(z) {
    var y = subVectors(z, mulMatrixVector(this.H, this.x));
    var S = addMatrices(this.R, mulMatrixMatrix(mulMatrixMatrix(this.H, this.P), transposeMatrix(this.H)))
    var K = mulMatrixMatrix(mulMatrixMatrix(this.P, transposeMatrix(this.H)), inv(S));
    this.x = addVectors(this.x, mulMatrixVector(K, y));
    var temp = mulMatrixMatrix(K, this.H);
    temp = subMatrices(eye(size(temp, 1)), temp);
    this.P = mulMatrixMatrix(mulMatrixMatrix(temp, this.P), transposeMatrix(temp));
    this.P = addMatrices(this.P, mulMatrixMatrix(mulMatrixMatrix(K, this.R), transposeMatrix(K)));
    return {
      x: this.x,
      P: this.P
    };
  }

  error_ellipse() {
    var temp = array2mat([[this.P.val[0], this.P.val[1]], [this.P.val[this.P.n], this.P.val[this.P.n + 1]]]);
    var E = eig(temp, true);
    var lambd1 = E.V[0];
    var lambd2 = E.V[1];
    var large_val = lambd2;
    var small_val = lambd1;
    var i = 1;
    if (lambd1 > lambd2) {
      large_val = lambd1;
      small_val = lambd2;
      i = 0;
    }
    var angle = Math.atan2(E.U[i], E.U[2 + i]);
    if (angle < 0.0) {
      angle = angle + 2 * Math.PI;
    }
    return {
      maj: 2 * Math.sqrt(5.991 * large_val),
      min: 2 * Math.sqrt(5.991 * small_val),
      rot: angle * 180.0 / (2 * Math.PI)
    };
  }
}

var mouseXpos = 0;
var mouseYpos = 0;

var app = new Vue({
  el: '#app',
  data: {
    settings: {
      fps: 5,
      model_noise: 10,
      meas_noise: 20
    }
  }
});

var svg = d3.select('body').append('svg')
  .attr('width', '100%')
  .attr('height', 800)
  .attr('stroke', 'black')
  .style('border-style', 'solid')
  .on('mousemove', function() {
    mouseXpos = d3.mouse(this)[0];
    mouseYpos = d3.mouse(this)[1];
  });
svg.append("ellipse")
  .attr("id", "priori")
  .attr("fill-opacity", 0.2);
svg.append("ellipse")
  .attr("id", "posteriori")
  .attr("fill", "steelblue")
  .attr("fill-opacity", 0.5);
svg.append("circle")
  .attr("id", "true")
  .attr("fill", "red");
svg.append("circle")
  .attr("id", "meas")
  .attr("fill", "steelblue");

var fps = app.settings.fps;
var fpsInterval = 1000.0 / fps;

// constant velocity model
F = zeros(4, 4);
F.val[0] = 1.0;
F.val[2] = 0.001 * fpsInterval;
F.val[5] = 1.0;
F.val[7] = 0.001 * fpsInterval;
F.val[10] = 1.0;
F.val[15] = 1.0;

H = zeros(2, 4);
H.val[0] = 1.0;
H.val[5] = 1.0;

x0 = zeros(4);
P0 = mulScalarMatrix(100.0, eye(4, 4));

Q = mulScalarMatrix(app.settings.model_noise * app.settings.model_noise, eye(4, 4));
Q.val[10] = app.settings.model_noise * app.settings.model_noise / 10.0;
Q.val[15] = app.settings.model_noise * app.settings.model_noise / 10.0;
R = mulScalarMatrix(app.settings.meas_noise * app.settings.meas_noise, eye(2, 2));

var kf = new KalmanFilter(F, H, x0, P0, Q, R);

var last = null;

function step(timestamp) {

  window.requestAnimationFrame(step);

  var dt = timestamp - last;
  var fps = app.settings.fps;
  var fpsInterval = 1000.0 / fps;

  // Only update if fpsInterval has passed.
  if (dt > fpsInterval) {
    last = timestamp;
    //console.log('dt: ', dt);
    var result = kf.predict();
    var ell = kf.error_ellipse();
    d3.select('#priori')
      .attr("cx", result.x[0])
      .attr("cy", result.x[1])
      .attr("rx", ell.maj)
      .attr("ry", ell.min)
      .attr("transform", "rotate(" + ell.rot.toString() + ")");
    var z = ones(2);
    z[0] = mouseXpos + app.settings.meas_noise * Math.random() - 0.5 * app.settings.meas_noise;
    z[1] = mouseYpos + app.settings.meas_noise * Math.random() - 0.5 * app.settings.meas_noise;
    d3.select('#true')
      .attr("cx", mouseXpos)
      .attr("cy", mouseYpos)
      .attr("r", 3);
    d3.select('#meas')
      .attr("cx", z[0])
      .attr("cy", z[1])
      .attr("r", 3);
    result = kf.update(z);
    var ell = kf.error_ellipse();
    d3.select('#posteriori')
      .attr("cx", result.x[0])
      .attr("cy", result.x[1])
      .attr("rx", ell.maj)
      .attr("ry", ell.min)
      .attr("transform", "rotate(" + ell.rot.toString() + ")");
  }
};

// Kick it off
window.requestAnimationFrame(step);

Kalman Filter Demonstration
===========================

This is a simple demo of the linear Kalman filter.

Run with:

```sh
$ node app.js
```
